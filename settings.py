ROOT = "/home/dann/workspace/python/virtual_environments/3.5.5/project_chat_bot"

DEFAULT_USERNAME = "default_user"
DEFAULT_DATABASE = "%s/users/default_user/default_user.sqlite" % (ROOT)

TEST_USER = "@@test_user"
TEST_AIML = "%s/context/@@test/@@test.aiml" % (ROOT)
TEST_DATABASE = "%s/users/@@test/@@test.sqlite" % (ROOT)

import logging, datetime
DATE = datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')

LOGGER = logging.Logger('xorva_logger')
LOGGER.setLevel(logging.DEBUG)

debug_log = logging.FileHandler('%s/logs/%s_debug.log' % (ROOT, DATE), mode='w')
debug_log.setLevel(logging.DEBUG)

info_log = logging.FileHandler('%s/logs/%s_info.log' % (ROOT, DATE), mode='w')
info_log.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s %(message)s', '[%m/%d/%Y %I:%M:%S]')
debug_log.setFormatter(formatter)
info_log.setFormatter(formatter)

LOGGER.addHandler(debug_log)
LOGGER.addHandler(info_log)