import sys
sys.path.append('./models')
sys.path.append('./logs')
sys.path.append('./context')

from chat_bot import ChatBot

ChatBot().run()