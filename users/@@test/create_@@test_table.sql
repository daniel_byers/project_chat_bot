DROP TABLE IF EXISTS '@@test_user';

CREATE TABLE '@@test_user'(
  context VARCHAR(50) PRIMARY KEY UNIQUE,
  memory VARCHAR(255)  
);

INSERT INTO '@@test_user' (context, memory)
VALUES ('@@test', '{"@@test": {"@@test_entity0": {"@@test_attribute0":"@@test_value0", "@@test_attribute1":"@@test_value1"}, "@@test_entity1": {"@@test_attribute2":"@@test_value2"}}}');
