from memory import Memory
import json, threading, sys
sys.path.append('..')
from settings import LOGGER
from collections import defaultdict

class MemoryFactory(object):
  '''Responsible for building Memory objects and their containers'''

  def make_memory(self, context, entity, attribute, value):
    '''Returns a new Memory object'''
    return Memory(args=(context, entity, attribute, value,))

  def parse(self, memorybank):
    '''Creates a list of memories from the given JSON string'''

    memory_dict = json.JSONDecoder(object_pairs_hook=self.__multdict).decode(memorybank)

    LOGGER.debug('[MF] [MEMORYDICT] %s' % memory_dict)

    memories = []
    for context, entities in memory_dict.items():
      for entity, descriptors in entities.items():
        for attribute, value in descriptors.items():
          memories.append(Memory(args=(context, entity, attribute, value)) )
    return memories

  def remove_duplicate_memories(self, old_memories, new_memories):
    '''Compares a list of old memories to a list of new memories and remove dulplicates'''

    for old_memory in old_memories:
      for new_memory in new_memories:
        if new_memory == old_memory:
          new_memories.remove(new_memory)
    return old_memories + new_memories

  def build_memorybank(self, memory_list):
    '''Builds a memorybank from a list of Memory objects, returns JSON string representation'''

    memory = memory_list.pop()
    mb = {memory.context: {memory.entity: {memory.attribute:memory.value}}}

    for memory in memory_list:
      try:              mb[memory.context][memory.entity][memory.attribute] = memory.value
      except KeyError:  mb[memory.context][memory.entity] = {memory.attribute:memory.value}
    return json.dumps(mb)

  # Top level context is hardcoded, no way to add more!
  def build_memorybank_v2(self, memory_list):
    '''Creates similar build_memorybank_v1 but doesnt coalesce entities. This is a stepping stone
    between the initial design and the final ideology; memories mapped together following a schema'''
    if memory_list:
      json_str = '{"%s": {' % memory_list[0].context
      for memory in memory_list:
        json_str += '"%s": {"%s": "%s"},' % (memory.entity, memory.attribute, memory.value)
      json_str = json_str.rstrip(',') + '}}'
      return json_str

  def make_memories(self, utterance, memories, update_queue):
    '''Spawns a new thread to handle Memory creation'''

    make_thread = MemoryMakerThread(args=(utterance, memories, update_queue,))
    make_thread.start()

  # Private

  def __multdict(self, ordered_pairs):
    '''Convert duplicate keys values to lists.'''
    d_dict = defaultdict(list)
    for key, value in ordered_pairs: d_dict[key].append(value)

    # Unpack lists that have only 1 item
    for key, value in d_dict.items():
      if len(value) == 1: d_dict[key] = value[0]
    return dict(d_dict)

class MemoryMakerThread(threading.Thread):
  '''Thread to allow concurrent Memory creation alongside Xorva's running instance'''

  def __init__(self, args=()):
    self.args = args
    threading.Thread.__init__(self)

  def run(self):
    '''Extracts information using rules defined in the PreProcessingEngine and builds a list of
     Memory objects.'''

    utterance, memories, update_queue = self.args
    factory = MemoryFactory()
    new_memories = []

    # Implementations to retrieve information from the sentences.
    # TODO: This needs to be extracted into an object of its own.
    for tree in utterance.chunks:
      for subtree in tree.subtrees(lambda x: x.label() == 'Chunk0'):
        vals = self.__retrieve_memory_attributes(subtree, utterance.stop_words_removed)
        new_memories.append(Memory(args=(utterance.context, vals[1], vals[0], ' '.join(vals[2:]))))

      for subtree in tree.subtrees(lambda x: x.label() == 'Chunk1'):
        vals = self.__retrieve_memory_attributes(subtree, utterance.stop_words_removed)
        new_memories.append(Memory(args=(utterance.context, vals[0], vals[1], ' '.join(vals[2:]))))

      for subtree in tree.subtrees(lambda x: x.label() == 'Chunk2'):
        vals = self.__retrieve_memory_attributes(subtree, utterance.stop_words_removed)
        new_memories.append(Memory(args=(utterance.context, vals[0], vals[1], 'true')))

      for subtree in tree.subtrees(lambda x: x.label() == 'Chunk3'):
        vals = self.__retrieve_memory_attributes(subtree, utterance.stop_words_removed)
        new_memories.append(Memory(args=(utterance.context, vals[0], vals[1], 'not true')))

      for subtree in tree.subtrees(lambda x: x.label() == 'Chunk4'):
        vals = self.__retrieve_memory_attributes(subtree, utterance.stop_words_removed)
        numbers = {"one": 1, "two": 2, "three": 3, "four": 4, "five": 5, "six": 6, "seven": 7,
                   "eight": 8, "nine": 9, "ten": 10}
        number = 0
        try: number = int(vals[0])
        except ValueError: number = numbers[vals[0]]
        entity = self.__singularise_plural_nouns(vals[1], utterance.pos_tagged_tokens)
        attribute = self.__singularise_plural_nouns(vals[2], utterance.pos_tagged_tokens)
        for i in range(number):
          new_memories.append(Memory(args=(utterance.context, entity, attribute, vals[3+i])))

      for subtree in tree.subtrees(lambda x: x.label() == 'Chunk5'):
        new_memories.append(Memory(args=(utterance.context, vals[0], vals[1], vals[2])))

      # For Tests  
      for subtree in tree.subtrees(lambda x: x.label() == '@@test_chunk'):
        vals = self.__retrieve_memory_attributes(subtree, utterance.stop_words_removed)
        new_memories.append(Memory(args=(utterance.context, vals[0], vals[1], vals[2])))
      # ---------

    new_memories = factory.remove_duplicate_memories(memories, new_memories)

    LOGGER.debug('[MF] [NEW MEMORIES]')
    for mem in new_memories: LOGGER.debug(mem)

    if len(new_memories) > 0:
      mb = factory.build_memorybank(new_memories)
      update_queue.put(mb)

  # Private

  def __singularise_plural_nouns(self, word, pos_tagged_tokens):
    '''Takes a word from the list of values calculated during #run, checks that word against the 
    PoS-tagged words and if it is a plural noun, returns the stemmed version'''
    for token in pos_tagged_tokens:
      if token[0] == word and token[1] == 'NNS': return word[:-1]
    return word

  def __retrieve_memory_attributes(self, subtree, stop_words_removed):
    return [ node[0] for node    in subtree 
                     if  node[0] in stop_words_removed and node[0] not in ['JJS', 'POS'] ]