from sys import stdin, stdout, path
from response_generator import ResponseGenerator
from user import User
import settings, os, sqlite3, re
path.append(settings.ROOT)
from settings import LOGGER

class ChatBot(object):
  '''The main entry point for interacting with Xorva.'''

  def __init__(self, user=None, input_stream=stdin, output_stream=stdout):
    self.input_stream = input_stream
    self.output_stream = output_stream

    if not user:  user = self.__enable_user()
    self.response_generator = ResponseGenerator(user)

    LOGGER.info('[XORVA]  Xorva Initialised')
    LOGGER.info('[XORVA]  User: %s' % user.name)

    self.__print_response('Hello, %s' % user.name)

  def run(self):
    '''Starts Xorva running'''
    while True:
      user_input = self.__collect_utterance()
      if user_input.rstrip() == 'quit': break
      self.__print_response(self.response_generator.generate_response(user_input))

  # Private

  def __enable_user(self):
    '''Does a check to see if a user with the provided name exists, if not then creates it'''
    self.__print_response('Login... enter username (a-z A-Z _ only):')
    name = self.__collect_utterance()

    name = re.sub('[^a-zA-Z_@]', '', name).lower()

    directory = '%s/users/%s' % (settings.ROOT, name)
    database = '{0}/{1}.sqlite3'.format(directory, name)

    if not os.path.exists(directory):
      os.makedirs(directory)
      open(database, 'w').close()
      con = sqlite3.connect(database)
      con.execute('CREATE TABLE %s(context VARCHAR(50) PRIMARY KEY UNIQUE, memory VARCHAR(255))' % name)

    return User(name, database)

  def __collect_utterance(self):
    return self.input_stream.readline()

  def __print_response(self, response):
    self.output_stream.write('%s\n' % response)

if __name__ == '__main__':
  print("Xorva should be run from 'xorva.py' in the parent directory. Quitting...")
  sys.exit(0)