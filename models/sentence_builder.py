class SentenceBuilder(object):
  '''Formats the sentence with an Active Memory if its available.'''
  
  def build(self, sentence):
    if sentence[1]: # Active memory
      return sentence[0].format(entity=sentence[1].entity, attribute=sentence[1].attribute, value=sentence[1].value)
    else:
      return sentence[0]