class Memory(object):
  """Memory object to enable advanced chat"""
  def __init__(self, args=()):
    self.context, self.entity, self.attribute, self.value = args

  def __str__(self):
      return '''
        MEMORY
         ├─Context
         │    └─{context}
         ├─Entity
         │    └─{entity}
         ├─Attribute
         │    └─{attribute}
         └─Value
              └─{value}
      '''.format(context=self.context, entity=self.entity, attribute=self.attribute, value=self.value)
  
  def __eq__(self, other_memory):
    return self.__key() == other_memory.__key()

  def __hash__(self):
    return hash(self.__key())

  def __key(self):
    return (self.context, self.entity, self.attribute, self.value)