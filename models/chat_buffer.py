from queue import Queue

class ChatBuffer(Queue):
  '''Wraps a Queue to allow conversation without I/O'''
  
  def readline(self):
    return super().get()

  def write(self, message):
    super().put(message)