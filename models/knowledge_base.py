import aiml, sqlite3, re, random, sys
sys.path.append('..')
from settings import LOGGER, ROOT
from rogerean_response import RogereanResponse

class KnowledgeBase(object):
  """encapsulates the AIML kernel, Rogerean response mechanism and sqlite3 
  database into a single KnowledgeBase"""

  def __init__(self, user):
    self.user = user

    self.aiml_kernel = aiml.Kernel()
    self.__set_up_aiml(None)

    self.rogerean_kernel = RogereanResponse()

    connection = sqlite3.connect(self.user.database)
    self.cursor = connection.cursor()

  def read_memories(self, utterance):
    '''Scans database for any matching memories relating to given context and returns a JSON string
     of all results. Returns None if there are no memories.'''
    
    select_sql = "SELECT memory FROM '%s' WHERE context='%s'" % (self.user.name, utterance.context)
    
    LOGGER.debug('[KB] [MEMORY SEARCH] %s' % select_sql)
    
    self.cursor.execute(select_sql)
    try:              return self.cursor.fetchone()[0]
    except TypeError: return None

  def read_all_memories(self):
    '''Retrieves all memories for the current user'''

    select_sql = "SELECT memory FROM '%s'" % self.user.name

    LOGGER.debug('[KB] [MEMORY SEARCH] %s' % select_sql)

    self.cursor.execute(select_sql)
    memories = self.cursor.fetchall()
    if not memories:  return None
    else:             return memories[0][0]
    # else:             return [tupl[0] for tupl in memories]

  def read_sentence(self, utterance, memories):
    ''' Returns a tuple containing an active memory (if present) and the response generated for the
    utterance'''

    active_memory = self.__determine_active_memory(memories, utterance)
    search_params = self.__determine_search_parameters(utterance, active_memory)

    LOGGER.debug('[KB] [ACTIVE MEMORY] {0}'.format(active_memory))
    LOGGER.debug('[KB] [SEARCH PARAMS] %s' % search_params)

    if search_params: response = self.aiml_kernel.respond(search_params)
    else:             response = self.rogerean_kernel.respond(utterance.utterance)
    return (response, active_memory)

  def write(self, utterance, memorybank):
    '''Updates or inserts memory into the database'''
    upsert_sql = "INSERT OR REPLACE INTO '%s' (context, memory) VALUES ('%s',  '%s')" % (self.user.name, utterance.context, memorybank)
    
    LOGGER.debug('[KB] [UPSERT_SQL] %s' % upsert_sql)
    
    self.cursor.execute(upsert_sql)
    self.cursor.connection.commit()

  # Private

  def __set_up_aiml(self, utterance):
    self.aiml_kernel.resetBrain()
    self.aiml_kernel.verbose(False)
    self.aiml_kernel.setPredicate('name', self.user.name)
    if utterance: self.aiml_kernel.learn('{root}/context/{context}/{context}.aiml'.format(root=ROOT, context=utterance.context))
      
  # Returns an active memory or none if there aren't any.
  def __determine_active_memory(self, memories, utterance):
    try:
      pattern = re.compile(utterance.pattern)
      related_memories = list(filter(lambda memory: pattern.search(memory.entity), memories))
      if related_memories:  return random.choice(related_memories)
      else:                 return random.choice(memories)
    except (IndexError, TypeError) as e:
      return None

  # Returns the query string used to retrieve the sentence from the knowledge environment.
  # One of three outcomes have occurred at this stage:
  #   Outcome 1: There is no context in the utterance; utterance used for rogerean response.
  #   Outcome 2: Context has been deduced, however there are no related memories; context specific
  #              brain loaded into AIML Kernel and utterance is supplied as search parameter.
  #   Outcome 3: Context has been deduced and related memories exist in the database; response will 
  #              be from context-tagsearch after the "entity" and "attribute" of the active
  #              memory have supplied as tags.
  def __determine_search_parameters(self, utterance, active_memory):
    if utterance.context:
      self.__set_up_aiml(utterance)
      if active_memory:
        return "XTAGSEARCH {entity} {attribute}".format(
                  entity=active_memory.entity, attribute=active_memory.attribute) # Outcome 3
      else:
        return utterance.utterance # Outcome 2
    else: 
      return None # Outcome 1