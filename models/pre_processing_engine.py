import re, os, ast, sys, nltk, pprint
sys.path.append('..')
from settings import ROOT, LOGGER 
from utterance import Utterance
from nltk.corpus import stopwords
# from pycorenlp import StanfordCoreNLP

class PreProcessingEngine(object):
  """Populates the fields of Utterance created from new input"""
  def __init__(self):
    self.context_keywords = self.__load_context_keywords()
    # self.nlp = StanfordCoreNLP('http://localhost:9000')

  def scan(self, user_input):
    """Returns an Utterance object with pre-processing results."""
    utterance = Utterance(user_input)
    utterance = self.replace_punctuation(utterance)
    utterance = self.expand_contractions(utterance)
    utterance = self.tokenise(utterance)
    utterance = self.context(utterance)
    utterance = self.remove_stop_words(utterance)
    utterance = self.part_of_speech_tags(utterance)
    utterance = self.recognise_named_entities(utterance)
    utterance = self.chunk_it(utterance)
    # utterance = self.determine_coreference(utterance)   For future use.
    utterance.utterance = utterance.utterance.lower()
    return utterance

  def replace_punctuation(self, utterance):
    '''Substitutes unwanted characters and substitues end of sentences for full-stops.'''
    utterance.utterance = re.sub(r'[^a-zA-Z0-9\s\'@@]', '', utterance.utterance)
    # utterance.utterance = re.sub(r'[,!?]', '.', utterance.utterance)
    return utterance

  def expand_contractions(self, utterance):
    '''Splits contracted words into their subsequent parts'''

    substitutions = {
      "don't": ["do", "not"],
      "can't": ["can", "not"],
      "won't": ["will", "not"],
      "we're": ["we", "are"],
      "how're": ["how", "are"],
      "you're": ["you", "are"],
      "i'm": ["I", "am"],
      "he's": ["he", "is"],
      "she's": ["she", "is"],
      "that's": ["that", "is"],
      "what's": ["what", "is"],
      "it's": ["it", "is"],
      "must've": ["must", "have"],
      "should've": ["should", "have"],
      "could've": ["could", "have"],
      "would've": ["would", "have"],
      "\bi\b": ["I"]
      # "'s": [""] Sometimes useful as it denotes a possessive; brother's, team's, work's.
    }
    for word, substitution in substitutions.items():
      utterance.utterance = re.sub(word, ' '.join(substitution), utterance.utterance, flags=re.I)
    return utterance

  def tokenise(self, utterance):
    '''Splits a sentence into it's tokens (words)'''

    utterance.sentences = nltk.tokenize.sent_tokenize(utterance.utterance)
    utterance.tokens = utterance.utterance.split()
    return utterance

  def context(self, utterance):
    '''Attempts to match words in the utterance by the loaded keyword dictionary'''

    for token in utterance.tokens:
      for context, words in self.context_keywords.items():
        pattern = re.compile(context, re.IGNORECASE)
        if pattern.search(token):
          LOGGER.debug('[PPE] [PATTERN MATCHED] %s' % pattern.pattern)
          utterance.context = context
          utterance.pattern = pattern.pattern
          return utterance
        for word in words:
          pattern = re.compile(word, re.IGNORECASE)
          if pattern.search(token):
            LOGGER.debug('[PPE] [PATTERN MATCHED] %s' % pattern.pattern)
            utterance.context = context
            utterance.pattern = pattern.pattern
            return utterance
    return utterance

  def remove_stop_words(self, utterance):
    '''Remove common words such as "is", "and", etc.'''

    stop_words = set(stopwords.words('english'))
    utterance.stop_words_removed = [token for token in utterance.tokens if not token.lower() in stop_words]
    return utterance

  def part_of_speech_tags(self, utterance):
    '''Tags the sentence with its part of speech (verb, noun, etc.)'''

    utterance.pos_tagged_tokens = nltk.pos_tag(utterance.tokens)
    return utterance

  def recognise_named_entities(self, utterance):
    '''Located any entities in the sentence'''

    named_entities = nltk.ne_chunk(utterance.pos_tagged_tokens, binary=True)
    tmp_ne = []
    for st in named_entities.subtrees(lambda x: x.label() == 'NE'): tmp_ne.append(st.leaves())
    utterance.named_entities = [ne[0][0] for ne in tmp_ne]
    return utterance

  def chunk_it(self, utterance):
    '''Pre-determined rules for locating key information in the sentence from the Parts of Speech. 
    Used later in the MemoryFactory'''

    chunk_gram = r"""
      Chunk0: {<DT><NN><IN><PRP.*><JJ.*>?<NN><VBZ>(<NNP>|<CD><N.*>(<CD><N.*>)?)}
      Chunk1: {<PRP.?>(<VBP><DT>|<JJ.*>|<NN>)?<NN><[PV].*>(<NN>|<V.*>|<JJ>)(<VBZ>|<IN>)?(<N.*>|<JJ>|<CD><N.*>?|<CD><NN><CD><NN>)}
      Chunk2: {<PRP.*><JJS>?<NN><VBZ><JJ>}
      Chunk3: {<PRP.*><JJS>?<NN><VBZ><RB><JJ>}
      Chunk4: {<PRP.*><VBP>?<CD><NNS><VB.*|PRP\$|NNS>*<NNP>+(<CC><NNP>)?}
      Chunk5: {<PRP><VBP><DT><JJ.*|NN><VB.*><NNP>}
    """
    chunk_parser = nltk.RegexpParser(chunk_gram)
    utterance.chunks = [chunk_parser.parse(utterance.pos_tagged_tokens)]
    return utterance

  def determine_coreference(self, utterance):
    '''calculates co-reference between words in a sentence.'''

    this_coref = []
    out = self.nlp.annotate(utterance.utterance, properties={
            'annotators': 'tokenize,ssplit,pos,lemma,ner,parse,depparse,dcoref', 'outputFormat': 'json'})
    for index, corefs in out['corefs'].items():
      for coref in corefs:
        if coref['isRepresentativeMention'] == False:
          this_coref.append(
            ([coref['text'],coref['sentNum'],coref['headIndex'],coref['startIndex'],coref['endIndex']],
             [corefs[0]['text'],corefs[0]['sentNum'],corefs[0]['headIndex'],corefs[0]['startIndex'],corefs[0]['endIndex']]))
    utterance.corefs = this_coref
    return utterance

  # Private

  def __load_context_keywords(self):
    keywords = {}
    for dirname, dirs, files in os.walk('%s/context/' % ROOT):
      for filename in files:
        if filename == 'context_keywords.config':
          with open(os.path.join(dirname, filename)) as config_file:
            keyword_dict = ast.literal_eval(config_file.read())
            keywords = dict(list(keywords.items()) + list(keyword_dict.items()))

    LOGGER.debug('[PPE] [KEYWORDS] %s' % keywords)
    return keywords