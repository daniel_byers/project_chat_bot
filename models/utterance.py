class Utterance(object):
  """Encapsulates all the information for an Utterance, including it's response"""

  context = None
  pattern = None
  sentences = []
  tokens = []
  pos_tagged_tokens = []
  stop_words_removed = []
  named_entities = []
  chunks = []
  corefs = []
  response = None
  
  def __init__(self, raw_utterance):
    self.raw_utterance = self.utterance = raw_utterance.rstrip()

  def __str__(self):
    return '''
    ╔══════════════════════════════════════════════════════════════════════════════╗
        UTTERANCE\t{id}                                                       
    ╠══════════════════════════════════════════════════════════════════════════════╣
      RAW UTTERANCE:      {raw_utterance}         
      UTTERANCE:          {utterance}         
      CONTEXT:            {context}         
      PATTERN:            {pattern}
      SENTENCES:          {sentences}
      TOKENS:             {tokens}
      POS TAGS:           {pos_tagged_tokens}
      STOP WORDS REMOVED: {stop_words_removed}
      NAMED ENTITIES:     {named_entities}
      CHUNKS:             {chunks}
      COREFS:             {corefs}
      RESPONSE:           {response}
    ╚══════════════════════════════════════════════════════════════════════════════╝
    '''.format(
      id=id(self),
      raw_utterance=self.raw_utterance,
      utterance=self.utterance,
      context=self.context,
      pattern=self.pattern,
      sentences=self.sentences,
      tokens=self.tokens,
      pos_tagged_tokens=self.pos_tagged_tokens,
      stop_words_removed=self.stop_words_removed,
      named_entities=self.named_entities,
      chunks=self.chunks,
      corefs=self.corefs,
      response=self.response
    )