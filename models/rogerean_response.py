'''
This code represents the script from the initial Joseph Wiezenbaum paper:
Weizenbaum, Joseph. (1966). "ELIZA—a computer program for the study of natural language
communication between man and machine". Communications of the ACM. 9 (1), 36-45.

Constructed for elizabot.js by:
Landsteiner, Norbert. (2005). elizabot.js (Version 1.1) [Computer Application]. Available:
http://www.masswerk.at/elizabot/. Last Accessed: 9th Mar 2017.

Adapted by Daniel Byers for Python.

  Array of
  ["<key>", <rank>, [
    ["<decomp>", [
      "<reasmb>",
      "<reasmb>",
      "<reasmb>"
    ]],
    ["<decomp>", [
      "<reasmb>",
      "<reasmb>",
      "<reasmb>"
    ]],
    . . .
  ]]
'''

keywords = [
  [r"xstartupchecks", 100, [
    [r"(.*)", [
      "xstartupchecks"
    ]]
  ]],
  [r"@@test", 100, [
    [r"(.*)", [
      "@@test_response"
    ]]
  ]],
  [r"xnone", 0, [
   [r"(.*)", [
       "I'm not sure I understand you fully.",
       "Please go on.",
       "What does that suggest to you?",
       "Do you feel strongly about discussing such things?",
       "That is interesting.  Please continue.",
       "Tell me more about that.",
       "Does talking about this bother you?"
    ]]
  ]],
  [r"\bsorry\b", 0, [
   [r"(.*)", [
       "Please don't apologise.",
       "Apologies are not necessary.",
       "I've told you that apologies are not required.",
       "It did not bother me.  Please continue."
    ]]
  ]],
  [r"\bapologise\b", 0, [
   [r"(.*)", [
       "goto sorry"
    ]]
  ]],
  [r"\bremember\b", 5, [
   [r"(.*)i remember(.*)", [
       r"Do you often think of \2?",
       r"Does thinking of \2 bring anything else to mind?",
       "What else do you recollect?",
       r"Why do you remember \2 just now?",
       r"What in the present situation reminds you of \2?",
       r"What is the connection between me and \2?",
       r"What else does \2 remind you of?"
    ]],
   [r"(.*)do you remember(.*)", [
       r"Did you think I would forget \2?",
       r"Why do you think I should recall \2 now?",
       r"What about \2?",
       "goto what",
       r"You mentioned \2?"
    ]],
   [r"(.*)you remember(.*)", [
       r"How could I forget \2?",
       r"What about \2 should I remember?",
       "goto you"
    ]]
  ]],
  [r"\bforget\b", 5, [
   [r"(.*)i forget(.*)", [
       r"Can you think of why you might forget \2?",
       r"Why can't you remember \2?",
       r"How often do you think of \2?",
       "Does it bother you to forget that?",
       "Could it be a mental block?",
       "Are you generally forgetful?",
       r"Do you think you are suppressing \2?"
    ]],
   [r"(.*)did you forget(.*)", [
       "Why do you ask?",
       "Are you sure you told me?",
       r"Would it bother you if I forgot \2?",
       r"Why should I recall \2 just now?",
       "goto what",
       r"Tell me more about \2."
    ]]
  ]],
  [r"\bif\b", 3, [
   [r"(.*)if(.*)", [
       r"Do you think it's likely that \2?",
       r"Do you wish that \2?",
       r"What do you know about \2?",
       r"Really, if \2?",
       r"What would you do if \2?",
       r"But what are the chances that \2?",
       "What does this speculation lead to?"
    ]]
  ]],
  [r"\bdreamed\b", 4, [
   [r"(.*)i dreamed(.*)", [
       r"Really, \2?",
       r"Have you ever fantasized \2 while you were awake?",
       r"Have you ever dreamed \2 before?",
       "goto dream"
    ]]
  ]],
  [r"\bdream\b", 3, [
   [r"(.*)", [
       "What does that dream suggest to you?",
       "Do you dream often?",
       "What persons appear in your dreams?",
       "Do you believe that dreams have something to do with your problem?"
    ]]
  ]],
  [r"\bperhaps\b", 0, [
   [r"(.*)", [
       "You don't seem quite certain.",
       "Why the uncertain tone?",
       "Can't you be more positive?",
       "You aren't sure?",
       "Don't you know?",
       "How likely, would you estimate?"
    ]]
  ]],
  [r"\bname\b", 15, [
   [r"(.*)", [
       "I am not interested in names.",
       "I've told you before, I don't care about names -- please continue."
    ]]
  ]],
  [r"\bdeutsch\b", 0, [
   [r"(.*)", [
       "goto xforeign",
       "I told you before, I don't understand German."
    ]]
  ]],
  [r"\bfrancais\b", 0, [
   [r"(.*)", [
       "goto xforeign",
       "I told you before, I don't understand French."
    ]]
  ]],
  [r"\bitaliano\b", 0, [
   [r"(.*)", [
       "goto xforeign",
       "I told you before, I don't understand Italian."
    ]]
  ]],
  [r"\bespanol\b", 0, [
   [r"(.*)", [
       "goto xforeign",
       "I told you before, I don't understand Spanish."
    ]]
  ]],
  [r"xforeign", 0, [
   [r"(.*)", [
       "I speak only English."
    ]]
  ]],
  [r"\b(hello|hey|hi|howdy|greetings)\b", 0, [
   [r"(.*)", [
       "How do you do.  Please state your problem.",
       "Hi.  What seems to be your problem?"
    ]]
  ]],
  [r"\bcomputer\b", 50, [
   [r"(.*)", [
       "Do computers worry you?",
       "Why do you mention computers?",
       "What do you think machines have to do with your problem?",
       "Don't you think computers can help people?",
       "What about machines worries you?",
       "What do you think about machines?",
       "You don't think I am a computer program, do you?"
    ]]
  ]],
  [r"\bam\b", 0, [
   [r"(.*)am i(.*)", [
       r"Do you believe you are \2?",
       r"Would you want to be \2?",
       r"Do you wish I would tell you you are \2?",
       r"What would it mean if you were \2?",
       "goto what"
    ]],
   [r"(.*)i am(.*)", [
       "goto i"
    ]],
   [r"(.*)", [
       "I don't understand that."
    ]]
  ]],
  [r"\bare\b", 0, [
   [r"(.*)are you(.*)", [
       r"Why are you interested in whether I am \2 or not?",
       r"Would you prefer if I weren't \2?",
       r"Perhaps I am \2 in your fantasies.",
       r"Do you sometimes think I am \2?",
       "goto what",
       "Would it matter to you?",
       r"What if I were \2?"
    ]],
   [r"(.*)you are(.*)", [
       "goto you"
    ]],
   [r"(.*)are(.*)", [
       r"Did you think they might not be \2?",
       r"Would you like it if they were not \2?",
       r"What if they were not \2?",
       r"Are they always \2?",
       r"Possibly they are \2.",
       r"Are you positive they are \2?"
    ]]
  ]],
  [r"\byour\b", 0, [
   [r"(.*)your(.*)", [
       r"Why are you concerned over my \2?",
       r"What about your own \2?",
       r"Are you worried about someone else's \2?",
       r"Really, my \2?",
       r"What makes you think of my \2?",
       r"Do you want my \2?"
    ]]
  ]],
  [r"\bwas\b", 2, [
   [r"(.*)was i(.*)", [
       r"What if you were \2?",
       r"Do you think you were \2?",
       r"Were you \2?",
       r"What would it mean if you were \2?",
       r"What does ' \2 ' suggest to you?",
       "goto what"
    ]],
   [r"(.*)i was(.*)", [
       "Were you really?",
       r"Why do you tell me you were \2 now?",
       r"Perhaps I already know you were \2."
    ]],
   [r"(.*)was you(.*)", [
       r"Would you like to believe I was \2?",
       r"What suggests that I was \2?",
       "What do you think?",
       r"Perhaps I was \2.",
       r"What if I had been \2?"
    ]]
  ]],
  [r"\bi\b", 0, [
   [r"(.*)i (desire|want|need)(.*)", [
       r"What would it mean to you if you got \3?",
       r"Why do you want \3?",
       r"Suppose you got \3 soon.",
       r"What if you never got \3?",
       r"What would getting \3 mean to you?",
       r"What does wanting \3 have to do with this discussion?"
    ]],
   [r"(.*)i am(.*) (sad|unhappy|depressed|sick)(.*)", [
       r"I am sorry to hear that you are \3.",
       r"Do you think coming here will help you not to be \3?",
       r"I'm sure it's not pleasant to be \3.",
       r"Can you explain what made you \3?"
    ]],
   [r"(.*)i am(.*) (happy|elated|glad|better)(.*)", [
       r"How have I helped you to be \3?",
       r"Has your treatment made you \3?",
       r"What makes you \3 just now?",
       r"Can you explain why you are suddenly \3?"
    ]],
   [r"(.*)i was(.*)", [
       "goto was"
    ]],
   [r"(.*)i (belief|feel|think|believe|wish) i(.*)", [
       "Do you really think so?",
       r"But you are not sure you \3.",
       r"Do you really doubt you \3?"
    ]],
   [r"(.*)i(.*)(belief|feel|think|believe|wish)(.*)you(.*)", [
       "goto you"
    ]],
   [r"(.*)i am(.*)", [
       r"Is it because you are \2 that you came to me?",
       r"How long have you been \2?",
       r"Do you believe it is normal to be \2?",
       r"Do you enjoy being \2?",
       r"Do you know anyone else who is \2?"
    ]],
   [r"(.*)i cannot(.*)", [
       r"How do you know that you can't \3?",
       "Have you tried?",
       r"Perhaps you could \3 now.",
       r"Do you really want to be able to \3?",
       r"What if you could \3?"
    ]],
   [r"(.*)i do not(.*)", [
       r"Don't you really \2?",
       r"Why don't you \2?",
       r"Do you wish to be able to \2?",
       "Does that trouble you?"
    ]],
   [r"(.*)i feel(.*)", [
       "Tell me more about such feelings.",
       r"Do you often feel \2?",
       r"Do you enjoy feeling \2?",
       r"Of what does feeling \2 remind you?"
    ]],
   [r"(.*)i(.*)you(.*)", [
       r"Perhaps in your fantasies we \2 each other.",
       r"Do you wish to \2 me?",
       r"You seem to need to \2 me.",
       r"Do you \2 anyone else?"
    ]],
   [r"(.*)", [
       r"You say \1?",
       "Can you elaborate on that?",
       r"Do you say \1 for some special reason?",
       "That's quite interesting."
    ]]
  ]],
  [r"\byou\b", 0, [
   [r"(.*)you remind me of(.*)", [
       "goto alike"
    ]],
   [r"(.*)you are(.*)", [
       r"What makes you think I am \2?",
       r"Does it please you to believe I am \2?",
       r"Do you sometimes wish you were \2?",
       r"Perhaps you would like to be \2."
    ]],
   [r"(.*)you(.*)me(.*)", [
       r"Why do you think I \2 you?",
       r"You like to think I \2 you -- don't you?",
       r"What makes you think I \2 you?",
       r"Really, I \2 you?",
       r"Do you wish to believe I \2 you?",
       r"Suppose I did \2 you -- what would that mean?",
       r"Does someone else believe I \2 you?"
    ]],
   [r"(.*)you(.*)", [
       "We were discussing you -- not me.",
       r"Oh, I \2?",
       "You're not really talking about me -- are you?",
       "What are your feelings now?"
    ]]
  ]],
  [r"\byes\b", 0, [
   [r"(.*)", [
       "You seem to be quite positive.",
       "You are sure.",
       "I see.",
       "I understand."
    ]]
  ]],
  [r"\bno\b", 0, [
   [r"(.*)no one(.*)", [
       r"Are you sure, no one \2?",
       r"Surely someone \2 .",
       "Can you think of anyone at all?",
       "Are you thinking of a very special person?",
       "Who, may I ask?",
       "You have a particular person in mind, don't you?",
       "Who do you think you are talking about?"
    ]],
   [r"(.*)", [
       "Are you saying no just to be negative?",
       "You are being a bit negative.",
       "Why not?",
       "Why 'no'?"
    ]]
  ]],
  [r"\bmy\b", 2, [
   [r"(.*)my(.*)(family|mother|mom|father|dad|sister|brother|wife|children|child)(.*)", [
       "Tell me more about your family.",
       r"Who else in your family \4?",
       r"Your \3?",
       r"What else comes to your mind when you think of your \3?"
    ]],
   [r"(.*)my(.*)", [
       r"Does that have anything to do with the fact that your \2?",
       r"Lets discuss further why your \2.",
       r"But your \2.",
       r"Your \2?",
       r"Why do you say your \2?",
       "Does that suggest anything else which belongs to you?",
       r"Is it important to you that your \2?"
    ]]
  ]],
  [r"\bcan\b", 0, [
   [r"(.*)can you(.*)", [
       r"You believe I can \2 don't you?",
       "goto what",
       r"You want me to be able to \2.",
       r"Perhaps you would like to be able to \2 yourself."
    ]],
   [r"(.*)can i(.*)", [
       r"Whether or not you can \2 depends on you more than on me.",
       r"Do you want to be able to \2?",
       r"Perhaps you don't want to \2.",
       "goto what"
    ]]
  ]],
  [r"\bwhat\b", 0, [
   [r"(.*)", [
       "Why do you ask?",
       "Does that question interest you?",
       "What is it you really want to know?",
       "Are such questions much on your mind?",
       "What answer would please you most?",
       "What do you think?",
       "What comes to mind when you ask that?",
       "Have you asked such questions before?",
       "Have you asked anyone else?"
    ]]
  ]],
  [r"\bwho\b", 0, [
   [r"who(.*)", [
       "goto what"
    ]]
  ]],
  [r"\bwhen\b", 0, [
   [r"when(.*)", [
       "goto what"
    ]]
  ]],
  [r"\bwhere\b", 0, [
   [r"where(.*)", [
       "goto what"
    ]]
  ]],
  [r"\bhow\b", 0, [
   [r"how(.*)", [
       "goto what"
    ]]
  ]],
  [r"\bbecause\b", 0, [
   [r"(.*)", [
       "Is that the real reason?",
       "Don't any other reasons come to mind?",
       "Does that reason seem to explain anything else?",
       "What other reasons might there be?"
    ]]
  ]],
  [r"\bwhy\b", 0, [
   [r"(.*)why do not you(.*)", [
       r"Do you believe I don't \2?",
       r"Perhaps I will \2 in good time.",
       r"Should you \2 yourself?",
       r"You want me to \2?",
       "goto what"
    ]],
   [r"(.*)why can not i(.*)", [
       r"Do you think you should be able to \2?",
       r"Do you want to be able to \2?",
       r"Do you believe this will help you to \2?",
       r"Have you any idea why you can't \2?",
       "goto what"
    ]],
   [r"(.*)", [
       "goto what"
    ]]
  ]],
  [r"\beveryone\b", 2, [
   [r"(.*)(everyone|everybody|nobody|noone)(.*)", [
       r"Really, \2?",
       r"Surely not \2.",
       "Can you think of anyone in particular?",
       "Who, for example?",
       "Are you thinking of a very special person?",
       "Who, may I ask?",
       "Someone special perhaps?",
       "You have a particular person in mind, don't you?",
       "Who do you think you're talking about?"
    ]]
  ]],
  [r"\beverybody\b", 2, [
   [r"(.*)", [
       "goto everyone"
    ]]
  ]],
  [r"\bnobody\b", 2, [
   [r"(.*)", [
       "goto everyone"
    ]]
  ]],
  [r"\bnoone\b", 2, [
   [r"(.*)", [
       "goto everyone"
    ]]
  ]],
  [r"\balways\b", 1, [
   [r"(.*)", [
       "Can you think of a specific example?",
       "When?",
       "What incident are you thinking of?",
       "Really, always?"
    ]]
  ]],
  [r"\balike\b", 10, [
   [r"(.*)", [
       "In what way?",
       "What resemblence do you see?",
       "What does that similarity suggest to you?",
       "What other connections do you see?",
       "What do you suppose that resemblence means?",
       "What is the connection, do you suppose?",
       "Could there really be some connection?",
       "How?"
    ]]
  ]],
  [r"\blike\b", 10, [
   [r"(.*)(be|be|am|is|are|was)(.*)like (.*)", [
       "goto alike"
    ]]
  ]],
  [r"\bdifferent\b", 0, [
   [r"(.*)", [
       "How is it different?",
       "What differences do you see?",
       "What does that difference suggest to you?",
       "What other distinctions do you see?",
       "What do you suppose that disparity means?",
       "Could there be some connection, do you suppose?",
       "How?"
    ]]
  ]]

]

pres = {
  "recollect": "remember",
  "recall": "remember",
  "dreamt": "dreamed",
  "dreams": "dream",
  "maybe": "perhaps",
  "certainly": "yes",
  "machine": "computer",
  "machines": "computer",
  "computers": "computer",
  "were": "was",
  "same": "alike",
  "identical": "alike",
  "equivalent": "alike"
}

posts = {
  "am": "are",
  "your": "my",
  "me": "you",
  "myself": "yourself",
  "yourself": "myself",
  "i": "you",
  "you": "I",
  "my": "your",
  "i'm": "you are",
}

import re, time

class Rule(object):
  """Contains the decomposition rule and a list of reassemnly rules associated with a Key"""
  def __init__(self, decomposition_rule, reassembly_rules):
    self.decomposition_rule = re.compile(decomposition_rule)
    self.reassembly_rules = reassembly_rules
    self.rule_pointer = 0

  def retrieve_next_reassembly(self):
    '''The next response to provide'''

    try:
      rule = self.reassembly_rules[self.rule_pointer]
      self.rule_pointer += 1
    except IndexError as error:
      rule = self.reassembly_rules[0]
      self.rule_pointer = 1 # Set the pointer to 1 as the response generated right now was 0
    return rule

  def __str__(self):  return '\n  %s:  %s\n' % (self.decomposition_rule, self.reassembly_rules)

  def __repr__(self): return self.__str__()

class Key(object):
  """Holds the information of a Key; the pattern it matches, the associated rank and the rules to
   decompose and assembly the input"""
  def __init__(self, pattern, rank, rules):
    self.pattern, self.rank = re.compile(pattern), rank
    self.rules = [Rule(rule[0], rule[1]) for rule in rules]

  def __str__(self):
    return '''\
    \n Pattern:  %s\
    \n Rank:     %s\
    \n Rules:    %s
    ''' % (self.pattern, self.rank, self.rules)

  def __repr__(self): return self.__str__()

class RogereanResponse(object):
  """Response class when there is no context"""

  def __init__(self):
    self.keyword_list = [Key(keyword[0], keyword[1], keyword[2]) for keyword in keywords]

  def respond(self, input_str):
    ''' 1) Pre-process string,
        2) Build key stack by matching input string with keyword pattern,
        3) Sort the key stack in descending order so highest rank is at the top,
        4) Decompose string using decomposition rules
        5) if 'goto' is returned, find key relating to that goto
            decompose string with updated decomposition rule from new key
           else 
            assemble response from key's assembly rules
        6) post-process result '''

    input_str = self.__rogerean_pre_processing(input_str)

    key_stack = []
    for key in self.keyword_list:
      if key.pattern.search(input_str): key_stack.append(key)

    ranked_stack = sorted(key_stack, key=lambda x: x.rank, reverse=True)

    for key in ranked_stack: # (#1)
      for rule in key.rules: # (#2)
        if re.search(rule.decomposition_rule, input_str):
          next_rule = rule.retrieve_next_reassembly()

          # If the reassembly rule is a 'goto', we find the related key and insert that into the
          # ranked stack underneath the current key being investigated. This way, the next key to be
          # investigated by (#1) will be the goto key.
          if next_rule[:4] == 'goto':
            ranked_stack.insert(ranked_stack.index(key) + 1, self.__find_key(next_rule[5:]))
            break
          else:      
            # Filters additional spaces
            return re.sub('\s+', ' ', rule.decomposition_rule.sub(next_rule, input_str))
    
    # Finished the stack of Keys and no match is found so default 'xnone' is returned
    # Filters additional spaces
    return re.sub('\s+', ' ', self.__find_key('xnone').rules[0].retrieve_next_reassembly())
  
  # Private

  def __rogerean_pre_processing(self, input_str):
    for key, value in pres.items():
      input_str = re.sub(key, value, input_str)
    return input_str

  def __find_key(self, lookup_string):
    for key in self.keyword_list:
      if re.match(key.pattern, lookup_string): return key




# Optimisation tests
# Method one: Dictionary(key=pattern, value=Keys); pattern compiled on the fly at run time.
# Method two: List of Keys; all patterns compiled when object instantiated

# if __name__ == "__main__":
#   iterations = 1000000
#   rg = RogereanResponse()
#   start = time.time()
#   for x in range(iterations): rg.respond("why don't computer think")
#   end = time.time()
#   print("time taken: %f seconds for %d iterations" % (end - start, iterations))

# Results
# -------------------------------------------
# | Method  | Iterations  | Time (Seconds)  |
# -------------------------------------------
# |   1     |  1 000      |    0.049504     |
# |   1     |  1 000 0    |    0.454780     |
# |   1     |  1 000 00   |    4.504919     |
# |   1     |  1 000 000  |   44.515205     |
# -------------------------------------------
# |   2     |  1 000      |    0.033175     | 
# |   2     |  1 000 0    |    0.291681     | 
# |   2     |  1 000 00   |    2.756301     | 
# |   2     |  1 000 000  |   27.565440     | 
# -------------------------------------------