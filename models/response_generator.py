from knowledge_base import KnowledgeBase
from pre_processing_engine import PreProcessingEngine
from sentence_builder import SentenceBuilder
from memory_factory import MemoryFactory
import queue, sys
sys.path.append('..')
from settings import LOGGER


class ResponseGenerator(object):
  '''The controller object responsible for generating responses'''

  def __init__(self, user):
    self.pre_processing_engine = PreProcessingEngine()
    self.knowledge_base = KnowledgeBase(user)
    self.sentence_builder = SentenceBuilder()
    self.factory = MemoryFactory()
    self.update_queue = queue.Queue()

    # Initializing complete check
    self.generate_response('xstartupchecks')
    LOGGER.info('[XORVA]  Start-Up Checks Complete')

  def generate_response(self, user_input):
    '''generates a response for the given input'''

    # pre-process the utterance
    utterance = self.pre_processing_engine.scan(user_input)

    # determine context from database if context is empty after pre-processing
    # TODO: Extract into own object. See below.
    if not utterance.context: utterance = self.__search_for_context(utterance)
    
    memories = []

    if utterance.context:
      
      # retrieve any pre-existing memories from knowledge base.
      memories_json = self.knowledge_base.read_memories(utterance)

      LOGGER.debug('[RG] [MEMORIES JSON] %s' % memories_json)
      
      # build a list of memories from the JSON array.
      memories = self.factory.parse(memories_json) if memories_json else []

      # create memory objects for each piece of information.
      self.factory.make_memories(utterance, memories, self.update_queue)

    # retrieve the response sentence template from the knowledge base
    sentence = self.knowledge_base.read_sentence(utterance, memories)

    # format the response that is to be returned to the user
    utterance.response = self.sentence_builder.build(sentence)

    LOGGER.debug(utterance)

    LOGGER.info('[XORVA]  [UTTERANCE] "%s"' % utterance.raw_utterance.rstrip())
    LOGGER.info('[XORVA]  [RESPONSE] %s' % utterance.response)

    # print(utterance)
    
    # check update queue for pending memorybank updates and save each update.
    try:
      for i in range(self.update_queue.qsize()):
        memorybank = self.update_queue.get_nowait()
        LOGGER.debug('[RG] new memorybank %s ' % memorybank)
        self.knowledge_base.write(utterance, memorybank)
    except queue.Empty: pass

    return utterance.response

  # Private
    
  # TODO: Only looks for Named Entities at the moment because searching for any word would
  #       produce a large number of false positives. This functionality can be extracted into
  #       a new object, ContextSearch, and extended with more sophisticated search parameters.
  def __search_for_context(self, utterance):
    memories_json = self.knowledge_base.read_all_memories()
    if not memories_json:
      utterance.context = None
      utterance.pattern = None
      return utterance
    memories = self.factory.parse(memories_json)
    for memory in memories:
      if memory.entity in utterance.named_entities or memory.value in utterance.named_entities:
        utterance.context = memory.context
        utterance.pattern = memory.entity
    return utterance