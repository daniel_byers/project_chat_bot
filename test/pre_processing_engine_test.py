import unittest, sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
sys.path.append('%s/models' % settings.ROOT)
from pre_processing_engine import PreProcessingEngine
from utterance import Utterance

class PreProcessingEngineTest(unittest.TestCase):
  def setUp(self):
    self.engine = PreProcessingEngine()
    self.utterance = Utterance('@@test sentence for testing purposes')

  def test_scan(self):
    self.utterance = self.engine.scan(self.utterance.raw_utterance)
    self.assertEqual(self.utterance.context, '@@test')
    self.assertEqual(self.utterance.tokens, ['@@test', 'sentence', 'for', 'testing', 'purposes'])

  def test_tokenise(self):
    self.utterance = self.engine.tokenise(self.utterance)
    self.assertEqual(self.utterance.tokens, ['@@test', 'sentence', 'for', 'testing', 'purposes'])

  def test_context(self):
    self.utterance.tokens = ['@@test', 'sentence', 'for', 'testing', 'purposes']
    self.context = self.engine.context(self.utterance)
    self.assertEqual(self.utterance.context, '@@test')

  def test_expand_contractions(self):
    self.utterance.utterance = '@@test sentence can\'t test purposes'
    self.utterance = self.engine.expand_contractions(self.utterance)
    self.assertEqual(self.utterance.utterance, '@@test sentence can not test purposes')

  def test_remove_stop_words(self):
    self.utterance.tokens = ['@@test', 'sentence', 'for', 'testing', 'purposes']
    self.utterance = self.engine.remove_stop_words(self.utterance)
    self.assertEqual(self.utterance.stop_words_removed, ['@@test', 'sentence', 'testing', 'purposes'])

  def test_part_of_speech_tags(self):
    self.utterance.tokens = ['@@test', 'sentence', 'for', 'testing', 'purposes']
    self.utterance = self.engine.part_of_speech_tags(self.utterance)
    self.assertEqual(
      self.utterance.pos_tagged_tokens,
      [('@@test', 'JJS'),('sentence', 'NN'),('for', 'IN'),('testing', 'VBG'),('purposes', 'NNS')])

  def test_named_entity_recognition_no_matches(self):
    self.utterance.pos_tagged_tokens = [('@@test', 'JJS'),('sentence', 'NN'),('for', 'IN'),('testing', 'VBG'),('purposes', 'NNS')]
    self.utterance = self.engine.recognise_named_entities(self.utterance)
    self.assertEqual(self.utterance.named_entities, [])

  def test_named_entity_recognition_matches(self):
    self.utterance.pos_tagged_tokens = [('@@test', 'JJS'),('sentence', 'NN'),('for', 'IN'),('testing', 'VBG'),('purposes', 'NNS'), ('Daniel', 'NNP')]
    self.utterance = self.engine.recognise_named_entities(self.utterance)
    self.assertEqual(self.utterance.named_entities, ['Daniel'])