import unittest, sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
sys.path.append('%s/models' % settings.ROOT)
from response_generator import ResponseGenerator
from utterance import Utterance
from user import User

class ResponseGeneratorTest(unittest.TestCase):
  def setUp(self):
    user = User(settings.TEST_USER, settings.TEST_DATABASE)
    self.response_generator = ResponseGenerator(user)

  def test_generate_response(self):
    self.assertRegex(self.response_generator.generate_response('@@test'), r'entity is @@test_entity\d, attribute is @@test_attribute\d')