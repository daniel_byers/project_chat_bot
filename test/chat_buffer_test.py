import unittest, sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
sys.path.append('%s/models' % settings.ROOT)
from chat_buffer import ChatBuffer

class ChatBufferTest(unittest.TestCase):
  def setUp(self):
    self.buffer = ChatBuffer()

  def test_readline(self):
    self.buffer.put('test')
    self.assertEqual(self.buffer.readline(), 'test')

  def test_write(self):
    self.buffer.write('test')
    self.assertEqual(self.buffer.get(), 'test')