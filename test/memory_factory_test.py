import unittest, sys, os, queue
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
sys.path.append('%s/models' % settings.ROOT)
from memory_factory import MemoryFactory
from utterance import Utterance
from memory import Memory
from nltk.tree import Tree

class MemoryFactoryTest(unittest.TestCase):
  def setUp(self):
    self.factory = MemoryFactory()
    self.utterance = Utterance('This @@test string has a @@test_entity, a @@test_attribute and a @@test_value')
    self.utterance.context = '@@test'
    self.utterance.tokens = ['This', '@@test', 'string', 'has', 'a', '@@test_entity', ',', 'a', '@@test_attribute', 'and', 'a', '@@test_value']
    self.utterance.pos_tagged_tokens = [('@@test_entity', 'NN'), ('@@test_attribute', 'CD'), ('@@test_value', 'NN')]
    self.utterance.stop_words_removed = ['@@test_entity', '@@test_attribute', '@@test_value']
    self.utterance.chunks = [Tree('S', [Tree('@@test_chunk', [('This', 'DT'), ('@@test', 'JJS'), ('string', 'NN'), ('has', 'VBZ'), ('a', 'DT'), ('@@test_entity', 'NN'), ('a', 'DT'), ('@@test_attribute', 'NN'), ('and', 'CC'), ('a', 'DT'), ('@@test_value', 'NN')])])]

  def test_make_memory(self):
    self.assertIsInstance(self.factory.make_memory(
      '@@test', '@@test_entity', '@@test_attr', '@@test_val'),
      Memory)

  def test_memory_attributes(self):
    memory = self.factory.make_memory(
      '@@test', '@@test_entity', '@@test_attr', '@@test_val')
    self.assertEqual(
      [memory.context, memory.entity, memory.attribute, memory.value],
      ['@@test', '@@test_entity', '@@test_attr', '@@test_val'])

  def test_parse(self):
    memories = self.factory.parse(
      '{"@@test": {"@@test_entity0": {"@@test_attribute0": "@@test_value0", "@@test_attribute1": "@@test_value1"}, "@@test_entity1": {"@@test_attribute2": "@@test_value211"}}}')
    self.assertEqual(len(memories), 3)

  def test_remove_duplicate_memories(self):
    memory0 = self.factory.make_memory(
      '@@test0', '@@test_entity0', '@@test_attr0', '@@test_val0')
    memory1 = self.factory.make_memory(
      '@@test1', '@@test_entity1', '@@test_attr1', '@@test_val1')
    self.assertEqual(
      self.factory.remove_duplicate_memories([memory0], [memory0, memory1]),
      [memory0, memory1])

  # Obviously the regex in this test is broad. This is by design.. because it doesn't
  # matter which order the memories appear so long as they are under the correct entity.
  # The pattern matches that there are two entities within the context, each with one or
  # more descriptors.
  def test_build_memorybank(self):
    memory0 = self.factory.make_memory(
      '@@test', '@@test_entity0', '@@test_attr0', '@@test_val0')
    memory1 = self.factory.make_memory(
      '@@test', '@@test_entity1', '@@test_attr0', '@@test_val0')
    memory2 = self.factory.make_memory(
      '@@test', '@@test_entity1', '@@test_attr1', '@@test_val1')
    self.assertRegex(
      self.factory.build_memorybank([memory0, memory1, memory2]),
      '^{"[a-zA-Z0-9@@_]*":\s?{"[a-zA-Z0-9@@_]*":\s?{.*},\s?"[a-zA-Z0-9@@_]*":\s?{.*}}}')

  def test_make_memories(self):
    update_queue = queue.Queue()
    self.factory.make_memories(self.utterance, [], update_queue)
    self.assertEqual(update_queue.get(), '{"@@test": {"@@test_entity": {"@@test_attribute": "@@test_value"}}}')
    # self.assertRegex(update_queue.get(), '^{"[a-zA-Z0-9@@_]*":\s?{"[a-zA-Z0-9@@_]*":\s?{.*}}}')