import unittest, sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
sys.path.append('%s/models' % settings.ROOT)
from memory import Memory

class MemoryTest(unittest.TestCase):
  def setUp(self):
    self.memory = Memory(args=('@@test0', '@@test_entity0', '@@test_attr0', '@@test_val0',))

  def test_equality_pass(self):
    other_memory0 = Memory(args=('@@test0', '@@test_entity0', '@@test_attr0', '@@test_val0',))
    self.assertEqual(self.memory, other_memory0)

  def test_equality_fail(self):
    other_memory0 = Memory(args=('@@test0', '@@test_entity0', '@@test_attr1', '@@test_val0',))
    other_memory1 = Memory(args=('@@test0', '@@test_entity1', '@@test_attr0', '@@test_val0',))
    other_memory2 = Memory(args=('@@test1', '@@test_entity0', '@@test_attr0', '@@test_val0',))
    self.assertNotEqual(self.memory, other_memory0)
    self.assertNotEqual(self.memory, other_memory1)
    self.assertNotEqual(self.memory, other_memory2)