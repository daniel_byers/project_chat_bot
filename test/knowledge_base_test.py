import unittest, sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
sys.path.append('%s/models' % settings.ROOT)
from knowledge_base import KnowledgeBase
from utterance import Utterance
from collections import namedtuple
from user import User

class KnowledgeBaseTest(unittest.TestCase):
  def setUp(self):
    user = User(settings.TEST_USER, settings.TEST_DATABASE)
    self.knowledge_base = KnowledgeBase(user)
    self.utterance = Utterance('@@test_input')
    self.utterance.context = '@@test'
    self.utterance.pattern = '@@test_one'
    self.utterance.tokens = ['@@test_input']

  def tearDown(self):
    with open('%s/users/@@test/create_@@test_table.sql' % settings.ROOT) as file:
      self.knowledge_base.cursor.executescript(file.read())

  def test_read_memories(self):
    self.assertEqual(self.knowledge_base.read_memories(self.utterance), 
      '{"@@test": {"@@test_entity0": {"@@test_attribute0":"@@test_value0", "@@test_attribute1":"@@test_value1"}, "@@test_entity1": {"@@test_attribute2":"@@test_value2"}}}')

  def test_read_all_memories(self):
    self.assertRegex(self.knowledge_base.read_all_memories(),
      '^{"[a-zA-Z0-9@@_]*":\s?{"[a-zA-Z0-9@@_]*":\s?{.*},\s?"[a-zA-Z0-9@@_]*":\s?{.*}}}')

  def test_read_sentence_no_context(self):
    self.utterance.context = None
    self.assertEqual(self.knowledge_base.read_sentence(self.utterance, []), ('@@test_response', None))

  def test_read_sentence_context_no_memories(self):
    self.assertEqual(self.knowledge_base.read_sentence(self.utterance, []), ('Default contextual response', None))

  def test_read_sentence_context_memories(self):
    MockMemory = namedtuple('MockMemory', ['context', 'entity', 'attribute', 'value'])
    memory0 = MockMemory('@@test', '@@test_entity0', '@@test_attribute0', '@@test_value0')
    self.assertEqual(self.knowledge_base.read_sentence(self.utterance, [memory0]), ('entity is {entity}, attribute is {attribute}', memory0))

  def test_write(self):
    self.knowledge_base.write(self.utterance,
      '{"@@test_two": {"@@test_entity0": {"@@test_attribute0":"@@test_value0"}}}')
    self.assertEqual(self.knowledge_base.read_memories(self.utterance),
      '{"@@test_two": {"@@test_entity0": {"@@test_attribute0":"@@test_value0"}}}')

  def test_sequential_writes_update(self):
    self.knowledge_base.write(self.utterance,
      '{"@@test_two": {"@@test_entity0": {"@@test_attribute0":"@@test_value0"}}}')
    self.knowledge_base.write(self.utterance,
      '{"@@test_two": {"@@test_entity0": {"@@test_attribute0":"@@test_value0"}, "@@test_entity1": {"@@test_attribute1":"@@test_value1"}}}')
    self.assertEqual(
      self.knowledge_base.read_memories(self.utterance),
      '{"@@test_two": {"@@test_entity0": {"@@test_attribute0":"@@test_value0"}, "@@test_entity1": {"@@test_attribute1":"@@test_value1"}}}')