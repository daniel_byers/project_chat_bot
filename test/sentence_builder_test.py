import unittest, sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
sys.path.append('%s/models' % settings.ROOT)
from sentence_builder import SentenceBuilder
from collections import namedtuple

class SentenceBuilderTest(unittest.TestCase):
  def setUp(self):
    self.sentence_builder = SentenceBuilder()

  def test_build_no_memory(self):
    self.assertEqual(self.sentence_builder.build(('@@test', None)), '@@test')

  def test_build_with_memory(self):
    MockMemory = namedtuple('MockMemory', ['context', 'entity', 'attribute', 'value'])
    memory = MockMemory('@@test', '@@test_entity', '@@test_attribute', '@@test_value')
    self.assertEqual(
      self.sentence_builder.build(("{entity} {attribute} {value}", memory)),
      '@@test_entity @@test_attribute @@test_value')