import unittest, sys, os, threading
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import settings
sys.path.append('%s/models' % settings.ROOT)
from chat_bot import ChatBot
from chat_buffer import ChatBuffer

class ChatBotTest(unittest.TestCase):
  def setUp(self):
    self.buffer_in = ChatBuffer()
    self.buffer_out = ChatBuffer()
    
    self.thread = ChatBotThread(args=(self.buffer_out, self.buffer_in,))
    self.thread.start()

  def tearDown(self):
    self.buffer_out.write('quit')


class ChatBotThread(threading.Thread):
  def __init__(self, args=()):
    self.args = args
    threading.Thread.__init__(self)
    
  def run(self):
    buffer_out, buffer_in = self.args
    self.bot = ChatBot(input_stream=buffer_out, output_stream=buffer_in)
    self.bot.run()